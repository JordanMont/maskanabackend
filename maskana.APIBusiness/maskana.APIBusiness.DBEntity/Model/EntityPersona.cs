﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntity
{
    public class EntityPersona : EntityBase
    {
        public int IdUsuario { get; set; }
        public string Usuario { get; set; }
        public string Clave { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string RazonSocial { get; set; }
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public string Perfil { get; set; }

    }
}
