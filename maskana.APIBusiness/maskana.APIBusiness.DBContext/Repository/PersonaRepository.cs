﻿using Dapper;
using DBEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DBContext
{
    public class PersonaRepository : BaseRepository, IPersonaRepository
    {
        public EntityPersona GetUsuario(int pIdUsuario)
        {
            var persona = new EntityPersona();
            try
            {
                using (var db = GetSqlConnection())
                {
                    const string sql = @"usp_ObtenerUsuario";

                    persona = db.Query<EntityPersona>(sql,
                    commandType: CommandType.StoredProcedure).First();
                }
            }
            catch (Exception ex)
            {

            }
            return persona;
        }
    }
}
