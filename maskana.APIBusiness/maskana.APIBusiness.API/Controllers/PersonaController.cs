﻿using DBContext;
using DBEntity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NSwag.Annotations;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;

namespace UPC.APIBusiness.API.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    [Produces("application/json")]
    [Route("api/persona")]
    public class PersonaController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        protected readonly IPersonaRepository _PersonaRepository;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPersonaRepository"></param>
        public PersonaController(IPersonaRepository pPersonaRepository)
        {
            _PersonaRepository = pPersonaRepository;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Produces("application/json")]
        [AllowAnonymous]
        [HttpGet]
        [Route("getusuario")]
        public ActionResult GetUsuario(int id)
        {
            var ret = _PersonaRepository.GetUsuario(id);
            return Json(ret);
        }

    }
}
